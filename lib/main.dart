import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meme_generator_flutter/core/feature/memegen/presentation/meme_home.dart';

import 'core/feature/memegen/presentation/meme_home_bindings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      initialRoute: '/home',
      getPages: [
        GetPage(name: '/home', page: () => MemeHome(), binding: MemeHomeBindings()),
        GetPage(name: '/view', page: () => MemeHome()),
        // GetPage(name: '/share', page: () => MemeHome()),
      ],
    );
  }
}


