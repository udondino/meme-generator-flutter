import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:meme_generator_flutter/core/feature/memegen/presentation/meme_home_controller.dart';

class MemeHomeBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MemeHomeController());
  }
}