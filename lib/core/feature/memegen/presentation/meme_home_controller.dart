import 'package:get/get.dart';
import 'package:meme_generator_flutter/core/feature/memegen/entity/meme_response.dart';
import 'package:meme_generator_flutter/core/feature/memegen/providers/meme_providers.dart';

class MemeHomeController extends GetxController{

  List<Memes> memeResponse = List<Memes>().obs;

  getResponse() async {
    var json = await MemeProvider().getMemes();
    MemeResponse response = MemeResponse.fromJson(json.body);
    print(response.data.memes.length);
  }

  getMemes() {
    MemeProvider().getMemes().then((value) {
    MemeResponse response = MemeResponse.fromJson(value.body);
    memeResponse =  response.data.memes;
    print(memeResponse);
    });

  }

  @override
  void onInit() {
    // TODO: implement onInit
    print("helo");
    // print(MemeProvider().getMemes().then((value) => print(value)));
    getMemes();
    super.onInit();


  }
}