import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import 'meme_home_controller.dart';

class MemeHome extends GetView<MemeHomeController> {
  @override
  Widget build(BuildContext context) {
    controller.onInit();
    // List<Memes> memes = controller.getMemes();
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Center(
              child: Text(
                "MimGenerator",
                style: TextStyle(color: Colors.black, fontSize: Get.width * 0.08),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Icon(
                Icons.refresh,
                color: Colors.black38,
              ),
            ),
          ],
        ),
        toolbarHeight: Get.height * 0.2,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.getMemes();
        },
        child: Container(
            child: Obx(
          () => GridView.builder(
            itemCount: controller.memeResponse != null ? controller.memeResponse.length : 10,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            itemBuilder: (BuildContext context, int index) {
              return Card(
                  child: new Image.network(
                controller.memeResponse[index].url,
                fit: BoxFit.cover,
              ),);
            },
          ),
        )),
      ),
    );
  }
}
