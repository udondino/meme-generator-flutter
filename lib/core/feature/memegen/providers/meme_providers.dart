import 'package:get/get.dart';
import 'package:meme_generator_flutter/core/feature/memegen/entity/meme_response.dart';

class MemeProvider extends GetConnect{

  Future<Response> getMemes() => get('https://api.imgflip.com/get_memes');

}